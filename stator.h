#include "ringdisc.h"

#define S_OFF 0
#define S_ON 1
#define S_ALL 2


class stator{ // class stator contains the position and properties of attached stators

	friend class ringdisc;

	public:

		stator(int pos){

			position = pos;
			endangle = 0;
			activationtime = 0;
			status = S_OFF;
		};

		void activate(double finalangle){
			endangle = finalangle;
			status = S_ON;
		}

		void deactivate(double ontime){
			activationtime = ontime;
			status = S_OFF;
		}

	protected:
		int position; // attching position in the ring
		mutable bool status; // energetic status of the stator, S_ON or S_OFF
		mutable double endangle; // angle to finish rotation 
		mutable double activationtime; // time to wait for a OFF stator to receive an ion and start to work

};
