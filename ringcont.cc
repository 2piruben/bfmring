//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// 
// CLASS TO SIMULATE A CONTNUOUS RING BINDING AND UNBINDING
// 
// Ruben Perez Carrasco                                  1-10-11
//
//////////////////////////////////////////////////////////////////

/* This class models the binding and unbinding of stators to the ring
taking into account that the stators can bind the ring continuously without
any discretisation.

The ring has a length "length" and the stators a length "1" so in the optimal case
the ring will have attached "length" number of stators. Thus "length" is equivalent to 
maximum number of stators that can be bound to the rotor.

To compute the positions at which there is enough space to bind a stator, the list of 
empty regions is stored in "elist" and each empty region "ee" has a position and length

In a simlar way the bound stators are stored in "olist" (occupied list)

Reactions are computed using the Gillespie algorithm where the propensity of detaching is kout*N and
the propensity of attaching is kin*(length of empty areas).
*/

#include "ringcont.h"


//// CONSTRUCTORS AND DESTRUCTOR

ringcont::ringcont(float l,float ki, float ko) //
// constructor of the ring of length l with kin and kout rates 
	{
	length=l;
	kin=ki; // rate of attaching per unit length
	kout=ko; // rate of detaching per unit length

	ee.position=0; // Initially the ring is empty so elist will contain one element which is a whole empty region of length "length"
	ee.length=length;
	elist.push_back(ee);

	r = gsl_rng_alloc (gsl_rng_mt19937);	// r contains the random numbers
	if (SEED<1){ // if the SEED is negative, it is generated from clock
		SEED = time(NULL);
	}
	gsl_rng_set (r,SEED);

	}

ringcont::~ringcont()
	{
	}

///// MEMBER FUNCTIONS

void ringcont::SetRing(int n){//	
// resets de ring in a empty or full state with kin and kout rates
// n can be EMPTY_RING or FULL_RING

	elist.clear();
	olist.clear();

	switch(n){
	
		case(EMPTY_RING): // No units initially attached
			ee.position=0;
			ee.length=length;
			elist.push_back(ee);
		break;

		case(FULL_RING): // Ring initially full with stators
			for (float i=0;i<length;i++) olist.push_back(i);
			ee.position=floor(length);
			ee.length=length-floor(length); // if length is not an integer, then there will be a small empty regionat the end
			elist.push_back(ee);
		break;

		default:
			cout<<"SetRing invalid argument\n";
			exit(1);
	}	
}

void ringcont::PrintOccupancy(ofstream &output){
// print positions of the bound stators to output	
	for(oit=olist.begin();oit!=olist.end();++oit) output<<*oit<<' ';	
//	cout<<'\n';
}

void ringcont::PrintOccupancy(){
// print positions of the bound stators to output	
	for(oit=olist.begin();oit!=olist.end();++oit) cout<<*oit<<' ';	
//	cout<<'\n';
}


void ringcont::PrintVacancy(ofstream &output){
// print positions and length of empty regions in the ring
	for(eit=elist.begin();eit!=elist.end();++eit) cout<<'('<<(*eit).position<<','<<(*eit).length<<") ";
	cout<<'\n';
}

void ringcont::PrintVacancy(){
// print positions and length of empty regions in the ring
	for(eit=elist.begin();eit!=elist.end();++eit) cout<<'('<<(*eit).position<<','<<(*eit).length<<") ";
	cout<<'\n';
}

int ringcont::GetOccupancy(){
// 	print number of bound stators
	return olist.size();
}

void ringcont::SetKin(double k){
// set the rate of attaching
	kin=k;
}
	
void ringcont::SetKout(double k){
// set the rate of detaching
	kout=k;
}


double ringcont::ReactionStep(double maxtau){
//	
	space=MeasureVacancy();	// space avaiable to attach an unit
	propin=space*kin; // propensity of attaching
	propout=olist.size()*kout; // propensity of detaching propostional to number of bound stators
	rnd1=gsl_ran_flat (r,0,1);
	rnd2=gsl_ran_flat (r,0,1);
	tau=1.0/(propout+propin)*log(1.0/rnd1); // time of next reaction
	if (tau<maxtau){
		if ((propout/(propin+propout))>rnd2) DettachingEvent(); // selection of the reaction (dettaching or attaching)
		else AttachingEvent();
		return tau;
	}
	else return tau*(-1.0);
}
	
double ringcont::DettachingEvent(){
	
	RandomDettaching(); // A random element is selected to be detached
	// this function sets the "oit" pointer to that element
	// Now we find the previous and posterior empty regions and set their respective pointers "eitmin" and "eitmax"
	// to eventually fuse them in a new empty region
	d2=length+1;  // d2 and d3 store the minimum and maximum distances possible distances
	d3=-1;
	for(eit=elist.begin();eit!=elist.end();eit++){ // Loop over each empty region
		d1=ringdistance(*oit,(*eit).position); // d1 is the distance between each candidate and the dettached element
		if(d1<d2){eitmin=eit; d2=d1;} // new nearer (to the left) element found
		if(d1>d3){eitmax=eit; d3=d1;} // new further (to the left) element found
	}
	d2=(*eitmin).position+(*eitmin).length-0.1;
	d2=ringdistance(*oit,d2);

	// Posible vacancy-occupancy scenarios
	if(eitmin==eitmax){ // Only one vacancy envolving the unit that is extended 1 unit
	(*eitmin).length+=1;
	}

	else if(d2>0.5&&d3<length-1.5){ //The occupancy was surrounded by units (no adjacent vacacny)
//		cout<<"New space\n";
		ee.position=*oit;
		ee.length=1;
		elist.push_back(ee); // A new vacancy is created
	}
	else if(d2>0.5){ //There is one occupancy to the left and not to the right
//		cout<<"Expanding to the left d2="<<d2<<'\n';
		(*eitmax).position=*oit; // update of the initial position and length of the new empty space
		(*eitmax).length+=1;
	}
	else if(d3<length-1.5){ //There is one occupancy to the right but not to the left
//		cout<<"Expanding to the right\n";
		(*eitmin).length+=1;
	}
	else{// There are two vacancies to each side (usual scenario)
//		cout<<"Removing and fusion\n";
		(*eitmin).length+=(*eitmax).length+1; // eitmin absorves eitmax and the new free unity space
		elist.erase(eitmax); // the absorved new empty space dissapears
	}

	d1=(*oit); // auxiliar saving of the value for function returning	
	olist.erase(oit); // the dettached unit dissapears
	return d1;
}
	
double ringcont::AttachingEvent(){
	
	double dock=RandomAttaching();
	// This function sets *eit to the empty space about to be divided by the new attaching
	// dock is the distance from *eit.position where the attaching will take place

	ee.position=inring(dock+1+(*eit).position); //position of the new empty space after the unit
	ee.length=(*eit).length-1-dock; // length of the new empty space after the stator is attached

	olist.push_back( (*eit).position+dock ); // new unit
	elist.push_back(ee); // new vacancy
	(*eit).length=dock; // resize of old vacancy

	return (*eit).position+rnd1; // return position of new element
}

void ringcont::RandomDettaching(){
	// Function to choose one random stator to detach from olist. It sets the dummy pointer "oit" to it
	int i=0; // auxiliar counter for dettaching unit selection
	list<double>::iterator oitrnd;
	rnd3=gsl_rng_uniform_int(r,olist.size());
	for(oit=olist.begin();oit!=olist.end();oit++){if (i==rnd3) oitrnd=oit; i++;}
	oit=oitrnd;
}

double ringcont::RandomAttaching(){
	// Function choosing the position of the ring to a new attachment.
	double xa=0; // auxiliar accumulative variable for attaching position selection
	list<emptyelement>::iterator eitrnd;	

	rnd1=gsl_ran_flat(r,0,MeasureVacancy());
	for(eit=elist.begin();eit!=elist.end();eit++){
		if ((*eit).length>=1){
			if((*eit).length-1+xa>rnd1){ eitrnd=eit; break;}
			else  xa+=(*eit).length-1;
		}
	}

	eit=eitrnd; // global pointer eit now points to the broke vacant space 
	return rnd1-xa; // distance from *eit.position where the attaching will take place

}

double ringcont::MeasureVacancy(){
	// returns the total empty length of the ring available to bind a stator. 
	// This is used to measure how probable is to bind a stator
	double freearea=0;
	for(eit=elist.begin();eit!=elist.end();eit++){
		if((*eit).length>=1) freearea+=(*eit).length-1;
		// note that the avialable for each empty region is ist length-1 due to steric reasons
	}
	return freearea;
}


// Auxiliar function to measure distance in the periodic ring
double ringcont::ringdistance(double b,double a){
	double x;
	if( (b-a)>0 ) x=b-a;
	else x=b-a+length;
	return x;
}
// Auxiliar function to take into account the periodic ring
// gets a position and takes the modulo_length of the position.
// It could be written better with just std::modulo?
double ringcont::inring(double a){
	double x=a;
	while(x>length) x-=length;
	while(x<0) x+=length;
	return x;
}

		



