///////////////////////////////////
//  RING.CC
//////////////////////////////////////
/////////////////////////////////////////////////////
//
//  determination of the probability of filling a poligon of 26 faces with peaces of 2 sites with an constant
//  attaching and detaching of peaces with a determined time probability distribution
//  
//
////////////////////////////////// Ruben Perez 6/09/11

using namespace std;

#include <fstream>
#include <math.h>
#include <iostream>
#include <vector>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_histogram.h>

int main(int argc,char *argv[]){

ofstream output;
output.open("output.out");

const int sites=26;
const double theta0=2.0*3.14159/26.0;
//int realizations=1000000;
double kin=0.05;
double kout0=0.5; // base ratio
double kout=kout0; // final ratio

const double totaltime=100000.0;

// biophysical parameters to compute velocity
float ld=0.00001; // friction coefficient
float kion = 0.05; // dwell time for an ion to start the stroke will be kion/N+t0 where N is the number of bound stators
float t0=0; 
const double tau1=4000; // torque produced by an ion crossing the stator


bool flig[sites]; // flig is an array describing the rotor ring with "sites" MotA proteins. 0 means empty 1 means full
bool *fligp[sites+3]; // array of pointers to flig to handle the periodic conditions of flig
vector<int> occupationlist; // list of the curent pieces attached
vector<int> emptylist; // list of the possible free spaces

vector<int>::iterator it;// dummy iterator

double tau; // next reaction time

double propin; // propensity of attaching
double propout; // propensity of dettaching
bool nextevent; // 0 for dettaching 1 for attaching
int tempsite; // temporal site variable

//bool tempstate=false; // temporal state variable
//double tin=0; // auxiliar time of reaction

float timerecordstep=10; // timestep for the output file
float nextrecordtime=timerecordstep; // time for the next recorded event
int recordoccupation[sites/2+1]; // vector containing the times that n sites are observed
int realizations=0;// will be the number of elements of recordoccupation
float naverage=0;// average number of motors

float actualvelocity=0; // motor velocity at a certain points
float sumvelocity=0; // sum of velocities at different times, it will be used to compute the average velocity as sumvelocity/(number of measures)


// Setting of RNG

int SEED=1250;
gsl_rng * r = gsl_rng_alloc (gsl_rng_mt19937);	// r contains the random numbers
gsl_rng_set (r,SEED);
double  rnd1,rnd2; // will contain continuous random numbers
int rnd3; // will contain integer random numbers

// Setting of histograms
const int nbins=50;
const int histosup=100;
gsl_histogram * h = gsl_histogram_alloc (nbins);
gsl_histogram_set_ranges_uniform (h,0,histosup);
	

//////////////////////////////////////////////////////////////////////////////
// BIG LOOP FOR DIFFERENT REALISATIONS

for(int ireal=0;ireal<=10;ireal++){
// This loop is used when different realisations are required.
// Here also different parameters can be explored

		ld *= 4; // For each Loop the friction is increased 
		sumvelocity=0; // reset of the velocity
		
// Initialization of Molecular components

	for(int i=0;i<sites;i++){
		 flig[i]=0; // Initialitation of empty flig ring
	 	fligp[i]=&flig[i];
	}
	// Setting of cyclic boundary conditions, the positions sites, sites+1 and sites+2 
	// point to the elements 0,1,2 of the array.
	fligp[sites]=&flig[0]; 
	fligp[sites+1]=&flig[1]; 
	fligp[sites+2]=&flig[2]; 

	occupationlist.clear(); // Initialize occupation list as an empty list

	emptylist.clear(); // Initialitation of the emptylist 
	for(int i=0;i<sites;i++) emptylist.push_back(i); // introduce all the possible empty sites to the occupationlist
	for(int i=0;i<(sites/2+1);i++) recordoccupation[i]=0; // reset of recordoccupation counter


	/////MAIN LOOP ///////////////////////////
	double time=0;
	nextrecordtime=0;
	while(time<totaltime){

	/////////////// Time to record

		while(time>nextrecordtime){ // when the actual time exceeds the "nextrecordtime", then the actual state is recorded
			recordoccupation[occupationlist.size()]++; // histogram with number of sites is updated
			nextrecordtime+=timerecordstep;
			if (occupationlist.size()!=0) actualvelocity=theta0/(kion/occupationlist.size()+ld*theta0/tau1+t0);
			else actualvelocity=0;
			sumvelocity+=actualvelocity;
		}

		if (occupationlist.size()!=0) actualvelocity=theta0/(kion/occupationlist.size()+ld*theta0/tau1+t0);
		else actualvelocity=0;

////////////// Determination of next event
///////////// The determination is set using the Gillespie algorithm where the propensity of 
///////////// attaching and dettaching is proportional to the occupied sites and empty pairs of sites		

		rnd1=gsl_ran_flat (r,0,1);
		rnd2=gsl_ran_flat (r,0,1);

		kout=kout0*actualvelocity;

		if(occupationlist.size()==0){ // If there are not stators...
			tau=1.0/(propin)*log(1.0/rnd1);	// tau is the time fof the next reaction
			time+=tau
			nextevent=1;
		}
	
		else{ // If there are stators...
			propout=kout*occupationlist.size();
			propin=kin*emptylist.size();
			tau=1.0/(propout+propin)*log(1.0/rnd1);
			time+=tau;
			if ((propout/(propin+propout))>rnd2) nextevent=0;
			else nextevent=1;
		}

///////////// If Dettaching (nextevent=0)
		if (nextevent==0){
			rnd3=gsl_rng_uniform_int(r,occupationlist.size()); // determination of the detaching unit
			tempsite=occupationlist[rnd3]; 
			*fligp[tempsite]=false;
			*fligp[tempsite+1]=false; // setting the new pair of positions as empty in flig
			it=occupationlist.begin()+rnd3; 
			occupationlist.erase(it); // removing the occupied position from occupationlist
			emptylist.push_back(tempsite); // the new free dimer space is added to the emptylist
			// Additionally if the occupied site was surrounded by empty spaces, this also adds
			// possible dimer attachments surrounding it
			if(*fligp[tempsite-1] == false) emptylist.push_back((tempsite-1+sites)%sites);
			if(*fligp[tempsite+2]== false) emptylist.push_back((tempsite+1)%sites);
		}


///////////// If attaching (nextevent=1)
		if (nextevent==1){
			rnd3=gsl_rng_uniform_int(r,emptylist.size()); // determination of the attaching position
			tempsite=emptylist[rnd3];
//		cout<<tempsite<<'\n';
			*fligp[tempsite]=true;
			*fligp[tempsite+1]=true; // setting the new pair of positions as occupied in flig
			occupationlist.push_back(tempsite); // update of occupationlist
			it=emptylist.begin()+rnd3;
			emptylist.erase(it); // remove of the occupied eleement from emptylist
		// Additionally when a dimer is attached if it was surrounded by empty spaces,
		// it also removes additional space of attaching
			it=emptylist.end();
			for(int j=(emptylist.size()-1);j>=0;j--){
				it--;
				if(emptylist[j]==((tempsite-1+sites)%sites)) emptylist.erase(it);
			else if(emptylist[j]==((tempsite+1+sites)%sites)) emptylist.erase(it);
			}
	
		}

	/////////  Analyzing data
	

/*
		if(tempstate!= *fligp[5]) switch(*fligp[5]){
			case true: tin=time;tempstate=true;break;
			case false: cout<<tin<<' '<<time<<' '<<time-tin<<'\n';gsl_histogram_increment(h,time-tin);tempstate=false;break;
		}
*/
/*
	output<<"Attach="<<nextevent<<"  on site:"<<tempsite<<"  with RND: "<<rnd3<<'\n';
	output<<"Emptylist(: "<<emptylist.size()<<")\n";
	for(int ll=0;ll<emptylist.size();ll++) output<<' '<<emptylist[ll];
	output<<'\n';
	output<<"Fulllist(: "<<occupationlist.size()<<")\n";
	for(int ll=0;ll<occupationlist.size();ll++) output<<' '<<occupationlist[ll];
	output<<"\n\n";
	output<<"Ring: ";
	for(int ll=0;ll<sites;ll++) output<<' '<<*fligp[ll];
	output<<"\n\n";
*/


}// End of realization loop

	realizations=0;
	naverage=0;

	for(int ll=0;ll<(sites/2+1);ll++){
//		output<<ll<<' '<<recordoccupation[ll]<<'\n';
		realizations+=recordoccupation[ll];
		naverage+=(1.0*ll)*recordoccupation[ll];
	}

	velocity/=realizations;
	naverage/=realizations;
	output<<ld<<' '<<velocity<<' '<<ld*velocity<<' '<<naverage<<'\n';
	output.flush();
	
}// End of main loops

	

//	output<<"# Number of Stators      Normalized probability       Standard error\n";
//	for(int i=0;i<sites/2+1;i++) output<<i<<' '<<(1.0*recordoccupation[i])/realizations<<' '
//				<<sqrt( (((1.0*recordoccupation[i])/realizations)*(1-(1.0*recordoccupation[i])/realizations))/realizations )<<'\n';
	output.close();


/*
	double inf;
	double sup;
	float count;

	gsl_histogram_get_range (h,1,&inf,&sup);
	gsl_histogram_scale(h,1.0/(gsl_histogram_sum(h)*(sup-inf)));

	for(int i=0;i<nbins;i++){
		gsl_histogram_get_range (h,i,&inf,&sup);
		count=gsl_histogram_get (h,i);
		output<<inf+(sup-inf)/2.0<<' '<<count<<'\n';
	}

	gsl_histogram_free (h);

	output.close();
*/	

}//main





