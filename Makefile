# COMPILACIÓN DE main

CFLAGS = -DNDEBUG -O -std=c++11 -stdlib=libc++ -Wall -msse2 
LFLAGS = -lm -lgsl -lgslcblas
GPP = g++
OBJECTS = BFMrun.o ringcont.o ringdisc.o

#Linking
BFMrun: $(OBJECTS)
	$(GPP) $(CFLAGS) $(LFLAGS) -o BFMrun $(OBJECTS) 

BFMrun.o: BFMrun.cc
	$(GPP) $(CFLAGS) -c BFMrun.cc

ringdisc.o: ringdisc.cc ringdisc.h
	$(GPP) $(CFLAGS) -c ringdisc.cc

ringcont.o: ringcont.cc ringcont.h
	$(GPP) $(CFLAGS) -c ringcont.cc
