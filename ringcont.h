
# ifndef RINGCONT_H
# define RINGCONT_H


#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <gsl/gsl_randist.h>

using namespace std;


#define FULL_RING 0
#define EMPTY_RING 1

	struct emptyelement{
	// An empty element is a empty section of the ring between to stators
	// the possible binding sites are a collection of emptyelements	
	// just emptyelements with size>1 will be able to allocate a stator
		float position;
		float length;
	};

class ringcont{


public:

	ringcont(float l,float in, float out); // Constructor of the ring with size l and reaction constants in and out
	~ringcont(); // Destructor
	void SetRing(int n); // Set the total occupation of the Ring to FULL_RING or EMPTY_RINGt
	void PrintOccupancy(ofstream &out); // Print occupancy status to file
	void PrintVacancy(ofstream &out); // Print occupancy status to file
	void PrintOccupancy(); // Print occupancy status to standard output
	void PrintVacancy(); // Print occupancy status to standard output

	int GetOccupancy(); // Get the number of stators attached
	void SetKin(double k); // Set the value of kin;
	void SetKout(double k); // Set the value of kout;
	double AttachingEvent(); // Attaching of a unit to the ring
	double DettachingEvent(); // Dettaching of a unit from the ring
	double MeasureVacancy(); // Available area for a dock
	double ReactionStep(double maxtau = INFINITY); // Realization of one reaction
	void PushHistogram();
	void SetHistogram(); // prepares the histogram to be used (allocates memory and sets to zero)
	void ResetHistogram();// sets histogram to zero (same as SetHistogram but without allocating memory)
	int GetHistogram(double* h);
	double NormalizeHistogram();
	double AverageHistogram(); // return the averageoccupation of the histogram
	double DeviationHistogram(); // return the average standard deviation of the histogram
	void PrintHistogram();
	
private:

	float length; // length of ring
	list <double> olist; // Occupation list
	list <emptyelement> elist; // Empty sites list (position, length)
	emptyelement ee; // auxiliar empty element
	list<double>::iterator oit; // iterator for elements of olist
	list<emptyelement>::iterator  eit,eitmin,eitmax; // iterator for elements of elist
	double kin; // single molecule reaction rate of attaching
	double kout; // single molecule reaction rate of dettaching
	double rnd1; // random number for reaction selection
	double rnd2; // random number for reaction time
	int rnd3; // random number for chosing dettaching
	double space; // empty space available
	double propin; // propensity of attaching reaction
	double propout; // propensity of dettaching reaction
	double tau; // last reaction time
	double d1,d2,d3; // auxiliar distances
	int SEED;
	gsl_rng * r; // container for the random numbers
	double ringdistance(double a,double b); // returns the angular position x inside the ring (0,length)
	double inring(double a); // returns absolute position in the ring
	void RandomDettaching(); // Selects a random member of olist an points oit to it;
	double RandomAttaching(); // Selects a random member of olist an points oit to it;
};

#endif
