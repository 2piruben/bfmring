#! /bin/sh
""":"
exec ipython -i $0 ${1+"$@"}
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
import os
import bisect

sbcolorcycle=sns.color_palette("deep") # Setting Colour palettes
sbcolorcycledark=sns.color_palette("dark")
clw = 2.0 # circle line width (also for ellipse)
talpha = 0.05 # alpha for trajectory
sns.set_style("ticks") # Set plot properties
sns.set_context("talk")
sns.despine()
newblack = sns.xkcd_rgb["charcoal"]


def loaddataold(foldern = "output/"):
	global Alist
	foldername = foldern
	Alist = [{"gl":"0.17","r":"300","colorlight":sbcolorcycle[2],"colordark":sbcolorcycledark[2],"data":[]},
		  {"gl":"0.8","r":"500","colorlight":sbcolorcycle[1],"colordark":sbcolorcycledark[1],"data":[]},
		  {"gl":"11.3","r":"1000","colorlight":sbcolorcycle[0],"colordark":sbcolorcycledark[0],"data":[]}]
	for file in os.listdir(foldername):
		if file[:-3] == "gl":
			if file[2:6] == "0.17":
				Alist[0]["data"].append(np.loadtxt(foldername+file))
			if file[2:5] == "0.8":
				Alist[1]["data"].append(np.loadtxt(foldername+file))
			if file[2:6] == "11.3":
				Alist[2]["data"].append(np.loadtxt(foldername+file))


def loaddatanew(foldern = "output/"):
	global Alist, conditions
	Alist = []
	conditions = {"gl":set(),"tstall1":set(),"tstall2":set(),"realisation":set(), "kin":set(), "kout":set()}
	for file in os.listdir(foldern):
		if file[-3:]==".in":
			d = {}
			filename = file[:-3]
			with open(foldern+file) as f:
				for line in f:
					(par, val) = line.split()
					d[par] = val
					if par in conditions:
						conditions[par].add(val)
				d["data"] = np.loadtxt(foldern+filename+".out")	
			Alist.append(dict(d))


def distribution():
	talpha = 0.6
	xpoint1 = 3500
	xpoint2 = 3800
	f,ax = plt.subplots(1,2)
	gls = ["0.17","0.8","11.3"]
	for igl, gl in enumerate(gls):
		Agl = Alist[igl] 
		print Agl["gl"]
		dataA =np.array([A[bisect.bisect(A[:,0],xpoint1)-1,1] for A in Agl["data"]])
		dataB =np.array([A[bisect.bisect(A[:,0],xpoint2)-1,1] for A in Agl["data"]])
		dataAB = dataB-dataA
		ax[0].hist(dataA,np.arange(-0.5,13.5,1),normed=True,
			 histtype="stepfilled", color=Agl["colorlight"]+(talpha,),
			 edgecolor=Agl["colordark"]+(talpha,), linewidth=clw, label="$\gamma_{"+Agl['r']+"}$")
		ax[1].hist(dataAB,np.arange(-8.5,8.5,1),normed=True,
			 histtype="stepfilled", color=Agl["colorlight"]+(talpha,),
			 edgecolor=Agl["colordark"]+(talpha,), linewidth=clw)
	ax[0].set_xlabel("Steady state num. stators")
	ax[0].legend(fontsize = 15)
	ax[1].set_xlabel("Change in num. of stators after stall")
	ax[0].set_ylabel("Prob. Density")

	ax[0].set_xticks(np.arange(0,13))
	ax[1].set_xticks(np.arange(-8,9))
	ax[0].set_xlim([-1,13])
	ax[1].set_xlim([-6,8])
	plt.savefig("Histogramsstall.pdf")
	plt.show()

def distributioncompare(Blist):

	talpha = 0.6
	xpoint1 = 3400
	f,ax = plt.subplots(1,3,figsize=(15,5))
	gls = ["0.17","0.8","11.3"]
	for igl, gl in enumerate(gls):
		Agl = Alist[igl]
		Bgl = Blist[igl]
		print Agl["gl"]
		dataA =np.array([A[bisect.bisect(A[:,0],xpoint1)-1,1] for A in Agl["data"]])
		print np.mean(dataA)
		dataB =np.array([A[bisect.bisect(A[:,0],xpoint1)-1,1] for A in Bgl["data"]])
		ax[igl].hist(dataB,np.arange(-0.5,13.5,1),normed=True,
			 histtype="stepfilled", color=Agl["colorlight"]+(0.0,),
			 edgecolor=Agl["colorlight"]+(1.0,), linewidth=clw, label="constant $k_{out}$")
		ax[igl].hist(dataA,np.arange(-0.5,13.5,1),normed=True,
			 histtype="stepfilled", color=Agl["colordark"]+(talpha,),
			 edgecolor=Agl["colordark"]+(0.0,), linewidth=clw, label="$k_{out}(\omega)$")
		ax[igl].legend(fontsize = 15)
		ax[igl].set_xticks(np.arange(0,13))
		ax[igl].set_ylim([0,0.4])
		sns.despine()
	ax[1].set_xlabel("Steady state number of stators")
	ax[0].set_ylabel("Prob. Density")
	f.subplots_adjust(wspace=0.3)
	f.tight_layout()
	plt.savefig("histogramskconstant.pdf")
	plt.show()


def trajectory():

	gls = ["0.17","0.8","11.3"]
	Npoints = 1000
	tstall1 = 3500
	tstall2 = 3800
	tfinal = 5500 	
	xaver = np.linspace(0,tfinal,Npoints)
	f,ax = plt.subplots(3,sharex=True,sharey=True)
	for igl, gl in enumerate(gls):
		ax[igl].fill_between(xaver[(xaver>tstall1) & (xaver<tstall2)],0,12.5,facecolor=newblack, alpha=0.2)
		for Agl in Alist:
			if Agl["gl"] == gl:
				for A in Agl["data"]:
					ax[igl].step(A[:,0],A[:,1],color=Agl["colorlight"]+(talpha,),linewidth = 0.5,where='post')
				yaver = np.zeros(Npoints)
				for i in range(Npoints):
					yaver[i] = np.mean([A[bisect.bisect(A[:,0],xaver[i])-1,1] for A in Agl["data"]]) 
				ax[igl].plot(xaver,yaver,color=Agl["colordark"])
				labelradius = Agl["r"]
		ax[igl].text(500,10,"$\gamma_{"+labelradius+"}$",fontsize = 30)
		ax[igl].set_xlim([0,tfinal])
		ax[igl].set_ylim([0,12.5])
		sns.despine()
	ax[1].set_ylabel("Number of stators", fontsize = 20)
	ax[2].set_xlabel("time (s)", fontsize = 20)		
	f.subplots_adjust(hspace=0)
	plt.savefig("trajectories.png")
	f.show()


def kincomp(foldername="output/"):

	dictlist=[{"name":"l1","colorlight":sbcolorcycle[3],"colordark":sbcolorcycledark[3],"label":"$\ell=1$"},
			  {"name":"l2","colorlight":sbcolorcycle[4],"colordark":sbcolorcycledark[4],"label":"$\ell=2$"},
			  {"name":"linf","colorlight":sbcolorcycle[5],"colordark":sbcolorcycledark[5],"label":"$\ell=\infty$"}]

	fig=plt.figure(figsize=(8,6))

	# Drawing equilibrium line
	xeq = np.linspace(0,0.01,100) # set of kin
	kinoriginal = 1.0/800.0
	kout = kinoriginal*(12-9)/(9)
	yeq = 12.0/(1+kout/xeq)
	plt.plot(xeq,yeq,'k-')

	# Drawing original value of kin
	yeq = np.linspace(0,12,3)
	xeq = np.ones_like(yeq)*kinoriginal
	plt.plot(xeq,yeq,'k--')


	for file in os.listdir(foldername):
		for dictdata in dictlist:
			if (file[:-4]==dictdata["name"]):
				A=np.loadtxt(foldername+file)
				plt.plot(A[:,0],A[:,1],'o',
				markerfacecolor=dictdata["colorlight"],markeredgecolor=dictdata["colordark"],
				label = dictdata["label"], mew=1.0)


	plt.xlabel("$k_{in}$", fontsize = 30)
	plt.ylabel("$\langle N \\rangle_{eq}^{stall}$",fontsize = 30)
	plt.legend(fontsize= 30, loc = 'lower right')
	sns.despine()
	plt.tight_layout()
	
	plt.savefig("stallNl.pdf")
	plt.show()


def kouttraj():

	gls = ["0.17","0.8","11.3"]
	Npoints = 1000
	tstall1 = 3500
	tstall2 = 3800
	tfinal = 5500 	
	xaver = np.linspace(0,tfinal,Npoints)
	f,ax = plt.subplots(3,sharex=True,sharey=True)
	for igl, gl in enumerate(gls):
		ax[igl].fill_between(xaver[(xaver>tstall1) & (xaver<tstall2)],0,0.004,facecolor=newblack, alpha=0.2)
		for Agl in Alist:
			if Agl["gl"] == gl:
				for A in Agl["data"]:
					ax[igl].step(A[:,0],A[:,4],color=Agl["colorlight"]+(talpha,),linewidth = 0.5,where='post')
				yaver = np.zeros(Npoints)
				for i in range(Npoints):
					yaver[i] = np.mean([A[bisect.bisect(A[:,0],xaver[i])-1,4] for A in Agl["data"]]) 
				ax[igl].plot(xaver,yaver,color=Agl["colordark"])
				labelradius = Agl["r"]
		ax[igl].text(500,10,"$\gamma_{"+labelradius+"}$",fontsize = 30)
		ax[igl].set_xlim([0,tfinal])
		ax[igl].set_ylim([0,0.004])
		ax[igl].set_yticks([0.0005,0.001,0.0015,0.002,0.0025,0.003,0.0035])
		sns.despine()
	ax[1].set_ylabel("$\\langle k_{out}\\rangle (\mathrm{s}^{-1})$", fontsize = 25)
	ax[2].set_xlabel("time (s)", fontsize = 20)		
	f.subplots_adjust(hspace=0)
	plt.savefig("kout.png")
	f.show()


def koutgamma():

	tstall2 = list(conditions['tstall2'])[0]
	liststall = sorted(list(conditions["tstall1"]))
	for t in liststall:
		koutplot = []
		for g in conditions["gl"]:
			print g, t
			kout = []
			for dat in Alist:
				if (dat["gl"]==g and dat["tstall1"]==t):
					row = np.where(dat["data"][:,0]==int(tstall2))
					if row[0]:
						kout.append(dat["data"][row[0],4])
						print row[0], dat["data"][row[0],4][0]
			if kout: # if not empty
				koutplot.append([float(g),np.mean(kout)])
		koutplot = np.array(koutplot)
		print koutplot
		label = "$"+str((3800-int(t))/60)+"\,\mathrm{min}$"
		plt.semilogx(koutplot[:,0],koutplot[:,1],'o',label=label)
	ax  = plt.gca()
#	handles, labels = ax.get_legend_handles_labels()
#	labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t))
#	ax.legend(handles, labels)
	plt.legend(fontsize=20)
	ax=plt.gca()
	ax.set_ylabel("$\\langle k_{out}\\rangle (\mathrm{s}^{-1})$", fontsize = 25)
	ax.set_xlabel("$\gamma $", fontsize = 25)
	ax.set_ylim([0,0.007])
	axtwin=ax.twinx()
	axtwin.set_ylim([0,7])
	axtwin.set_ylabel("$K_D$", fontsize = 25)

	plt.tight_layout()	
	plt.savefig("koutgamma.png")	
	plt.show()


def Naveromegatau():

	theta0 = 2*np.pi/27.0
	tdwell = 5.9E-4
	tau0 = 200
	gl0 = 0.1


	domega, dtau = 1, 10

	# generate 2 2d grids for the x & y bounds
	taus, omegas = np.mgrid[slice(0.0, 2000.0, dtau),
    	            slice(0.0, 250.0, domega)]
   	N = (gl0*omegas*(2.0*np.pi)+taus)/tau0*(1+tdwell*omegas*27)
   	plt.pcolor(omegas, taus, N, cmap='hsv',vmax=14)
   	ax = plt.gca()
   	ax.set_xlabel("$\omega (Hz)$",fontsize=25)
   	ax.set_ylabel("$\langle \\tau \\rangle (\mathrm{pN\,nm})$",fontsize=25)
   	axcb = plt.colorbar(ticks=[0,2,4,6,8,10,12,14])
   	plt.tight_layout()
   	plt.savefig("avertauomega.png")
   	plt.show()


def singleomegatau():

	theta0 = 2*np.pi/27.0
	tdwells = [4E-4, 6E-4, 8E-4]
	tau0 = 190
	gl0 = 0.02

	omegas = np.arange(0,250)
	for tdwell in tdwells:
		torques = tau0/(1+tdwell*omegas*27)
		plt.plot(omegas,torques,label="$t_{dwell}=$ "+str(tdwell)+" s")
	ax=plt.gca()
   	ax.set_xlabel("$\omega (Hz)$",fontsize=25)
   	ax.set_ylabel("$\langle \\tau \\rangle/N (\mathrm{pN\,nm})$",fontsize=25)
   	ax.set_ylim(0,300)
   	plt.legend(fontsize=20)
   	plt.tight_layout()
   	plt.savefig("singletorque.png")
   	plt.show()


def maxoccupancy():
# Creation of 2D contour plot showing the maximum occupancy for a couple of values kin kout

	kinlist =  sorted([float(x) for x in conditions['kin']])
	koutlist =  sorted([float(x) for x in conditions['kout']])

	f, ax = plt.subplots(2,sharex=True)


	positionkin = {}
	positionkout = {}

	for ikin,kin in enumerate(kinlist):
		positionkin[str(kin)] = ikin
	for ikout,kout in enumerate(koutlist):
		positionkout[str(kout)] = ikout

	Nmax = np.zeros((len(kinlist),len(koutlist)))
	Ndelta = np.zeros((len(kinlist),len(koutlist)))

	for el in Alist:
		#print el['data']
		Nmax[positionkin[el["kin"]],positionkout[el["kout"]]] = Nmax[positionkin[el["kin"]],positionkout[el["kout"]]] + el['data'][-1,1]
		Nbefore = next(row[1] for row in el['data'] if row[0]>3490) 
		Nafter = next(row[1] for row in el['data'] if row[0]>3790) 
		Ndelta[positionkin[el["kin"]],positionkout[el["kout"]]] = Ndelta[positionkin[el["kin"]],positionkout[el["kout"]]] + (Nafter-Nbefore)

	Nmax = (1.0*Nmax) / 50
	Ndelta = (1.0*Ndelta) / 50


	kinmesh, koutmesh = np.meshgrid(kinlist,koutlist)

	ax[0].pcolor(kinmesh,koutmesh,Nmax.T,cmap='rainbow',vmin=1,vmax=12)
	ax[1].pcolor(kinmesh,koutmesh,Ndelta.T,cmap='terrain',vmin=0,vmax=5)
	#f.colorbar()
	#ax[1].colorbar()
	plt.savefig('Nmax16.png')
	plt.show()
















					
