import numpy as np
from scipy.optimize import curve_fit,differential_evolution
from scipy.stats import linregress
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as cm
import matplotlib as mpl
from matplotlib.collections import PatchCollection
import seaborn as sns

sns.set_style("ticks") # seaborn styling (just for the look)
sns.set_context("talk") 
sns.despine() # no mirror axis

import os

theta0 = 2*np.pi/27.0
Nmax = 12
koutaver = 1.0/1000*3/9

def omegaeq(gamma, kin, theta0, kw, km, Nmax, tc, tau):
	omega = np.sqrt((kin+km)*(kin+km)+4*kin*(kin+kw)*Nmax*tc*tau/theta0/gamma)
	omega = omega - (kin + km)
	omega = omega / (2*(kin+kw)*tc/theta0)
	return omega	

def Neq(gamma, kin, theta0, kw, km, Nmax, tc, tau):
	omega = omegaeq(gamma, kin, theta0, kw, km, Nmax, tc, tau) 
	Ne = gamma / tau * omega * (1 + tc*omega/theta0)
	return Ne

def Nglobal(gamma0, gammaL, theta0, tc, tau0, tauex):
	return tauex*(1+gamma0/gammaL)/tau0*(1+tc/theta0*tauex/gammaL)

def delta(gamma, kin, theta0, kw, km, Nmax, tc, tau):
	omega = omegaeq(gamma, kin, theta0, kw, km, Nmax, tc, tau) 
	delta = 1.0/(1+tc*omega/theta0)
	return delta

def keff(gamma, kin, theta0, kw, km, Nmax, tc, tau):
# effective recruitment kin*N-<kout>(Nmax-N) when exiting an
# equilibrium position
	N = Neq(gamma, kin, theta0, kw, km, Nmax, tc, tau)


def Neqmin(x):
	dist = np.power(Neq(0.17, x[0],theta0,x[1],x[1]*x[2],Nmax,x[3],x[4])-3.2,2)
	dist = dist + np.power(Neq(0.8, x[0],theta0,x[1],x[1]*x[2],Nmax,x[3],x[4])-6.1,2)
	dist = dist + np.power(Neq(11.3, x[0],theta0,x[1],x[1]*x[2],Nmax,x[3],x[4])-8.7,2)
	dist = dist + np.power(x[0]*3.3-x[1]*x[2]*8.7,2)
	#dist = dist + np.power(Neq(0.8, x[0],theta0,x[1],x[1]*x[2],Nmax,x[3],x[4])-6.1,2)
	return dist


def Nglobalmin(x):

	dist = 0
	for el in data:
		# if len(el["traj"]["omega"])>0:
		for kk,omega in enumerate(el["traj"]["omega"]):
#			if el["experiment"]=="stall":
			tauex=omega*el["gamma"]
			Nexp=el["traj"]["N"][kk]
			dist = dist + min(np.power(Nexp-Nglobal(x[0], el["gamma"], theta0, x[1], x[2], tauex),2),8)
		
	return dist



def loaddata(foldername="datastator"):

	global data

	data = []


	gammafiles = {"stall1300.npy": 11.0, "stall500_25g.npy": 1.75, "stall500.npy": 0.8, 
	            "stall300_25g.npy": 0.39, "stall300.npy": 0.17, 
	            "StallRelease300_25PercGlyc.p": 0.39, "StallRelease500_25PercGlyc.p": 0.17}
	badtrajs = {"StallRelease300_25PercGlyc.p": [0,9], "StallRelease500_25PercGlyc.p": [36]}
	#value of gamma for each file            
	abstr = {"stall1300.npy": "_stall", "stall500_25g.npy": "", 
			"stall500.npy": "_stall", "stall300_25g.npy": "", "stall300.npy": "_stall",
			"StallRelease300_25PercGlyc.p": "", "StallRelease500_25PercGlyc.p": ""}
	itemreq = ["stall1300.npy", "stall500_25g.npy", 
			"stall500.npy", "stall300_25g.npy", "stall300.npy"]
	# files are not consistent with dictionary names. Fixing that filewise
				
	for file in os.listdir(foldername):
		if file[-3:]=="npy" or file[-2:]==".p" :
			print file
			if file in itemreq :
				tempdict = np.load(foldername+'/'+file).item()
			else :
				tempdict = np.load(foldername+'/'+file)
			for key in tempdict:
				if (file not in badtrajs) or (key not in badtrajs[file]):
					filedictbefore = {"experiment":"steady","file":file,"key":key}
					filedictafter = {"experiment":"stall","file":file,"key":key}

					if "drag_after_stall" in tempdict[key].keys():
						filedictafter["gamma"]=tempdict[key]["drag_after_stall"]*1.0E21
					elif "drag_Nms" in tempdict[key].keys():
						filedictafter["gamma"]=tempdict[key]["drag_Nms"]*1.0E21
					else:
						filedictafter["gamma"]=gammafiles[file]

					if "drag_before_stall" in tempdict[key].keys():
						filedictbefore["gamma"]=tempdict[key]["drag_before_stall"]*1.0E21
					elif "drag_Nms" in tempdict[key].keys():
						filedictbefore["gamma"]=tempdict[key]["drag_Nms"]*1.0E21
					else:
						filedictbefore["gamma"]=gammafiles[file]

					DS = tempdict[key]["statnum_after"][1:]-tempdict[key]["statnum_after"][:-1] # difference between stator shift
					ksi = [i for i,j in enumerate(DS) if j!=0] # array with positions of the jumps
					ksi = [0]+ksi
					trajafter = {"Dt":[],"N":[],"omega":[],"DN":[]}
					for iindex,indexk in enumerate(ksi[1:]):
						trajafter["omega"].append(tempdict[key]["fit_after"+abstr[file]+"_pNnm"][indexk-1]/filedictafter["gamma"])
						trajafter["N"].append(tempdict[key]["statnum_after"][indexk-1])
						trajafter["Dt"].append((ksi[iindex+1]-ksi[iindex])/1000.0) # 1000 FPS
						trajafter["DN"].append(DS[indexk])
					filedictafter["traj"]=dict(trajafter)
					


					DS = tempdict[key]["statnum_before"][1:]-tempdict[key]["statnum_before"][:-1]
					ksi = [i for i,j in enumerate(DS) if j!=0]
					ksi = [0]+ksi
					trajbefore = {"Dt":[],"N":[],"omega":[],"DN":[]}
					for iindex,indexk in enumerate(ksi[1:]):
						trajbefore["omega"].append(tempdict[key]["fit_before"+abstr[file]+"_pNnm"][indexk-1]/filedictbefore["gamma"])
						trajbefore["N"].append(tempdict[key]["statnum_before"][indexk-1])
						trajbefore["Dt"].append((ksi[iindex+1]-ksi[iindex])/1000.0) # 1000 FPS
						trajbefore["DN"].append(DS[indexk])						
					filedictbefore["traj"]=dict(trajbefore)	
					data.append(dict(filedictbefore))
					data.append(dict(filedictafter))			

def minimiseN():
	global xmin
	# x[0] gamma0,   x[1] chemical time, x[2] individual torque
	boundsdown = [0.00001,1E-4,80]  
	boundsup = [0.001,1E-2,500]
	diffev = differential_evolution(Nglobalmin, zip(boundsdown,boundsup),popsize=10,polish=True, tol=0.0001)
	print diffev.x
	print diffev.success
	xmin = diffev.x

def Naveromegatau():

	domega, dtau = 1, 10
	gamma0 = xmin[0]
	tdwell = xmin[1]
	tau0 = xmin[2]

	# generate 2 2d grids for the x & y bounds
	taus, omegas = np.mgrid[slice(0.01, 2000.0, dtau),
    	            slice(0.01, 250.0, domega)]
	N = Nglobal(gamma0, taus/(omegas*(2.0*np.pi)), theta0, tdwell, tau0, taus)
#   	N = (gl0*omegas*(2.0*np.pi)+taus)/tau0*(1+tdwell*omegas*27)
   	plt.pcolor(omegas, taus, N, cmap='hsv', vmax=14)

   	omegatauexp=[]
   	cmhsv = plt.get_cmap("hsv")
   	colorsin = []
   	colorsedge = []

	for el in data:
		if len(el["traj"]["omega"])>1:
			for kk,omega in enumerate(el["traj"]["omega"]):
				tauex = omega*el["gamma"]
				Nexp = el["traj"]["N"][kk]
			# if el["experiment"]=="stall":
			# 	omega=el["traj"]["omega"][0]
			# 	tauex=el["traj"]["omega"][0]*el["gamma"]
			# 	Nexp=el["traj"]["N"][0]
			# elif el["experiment"]=="steady":
			# 	omega=el["traj"]["omega"][-1]
			# 	tauex=el["traj"]["omega"][-1]*el["gamma"]
			# 	Nexp=el["traj"]["N"][-1]

				omegatauexp.append([omega,tauex])
   				color = np.array(cmhsv(Nexp/14.0))
   				colorsin.append(color)
   				colorsedge.append((color + [0.0,0.0,0.0,0.0])/2)
   	omegatauexp = np.array(omegatauexp)

   	ax = plt.gca()
   	ax.set_xlabel("$\omega (Hz)$",fontsize=25)
   	ax.set_ylabel("$\langle \\tau_L \\rangle (\mathrm{pN\,nm})$",fontsize=25)
   	axcb = plt.colorbar(ticks=[0,2,4,6,8,10,12,14])

   	plt.scatter(omegatauexp[:,0]/(2*np.pi),omegatauexp[:,1],c=colorsin,edgecolors=colorsedge)

   	ax.set_xlim([0.0,250.0])
   	ax.set_ylim([0.0,2000.0])


   	plt.tight_layout()
   	plt.savefig("avertauomega2.png")
   	plt.show()

def Naveromegatausingle():

   	cmhsv = plt.get_cmap("hsv")
   	colorsinbefore = []
   	colorsedgebefore = []
   	colorsinafter = []
   	colorsedgeafter = []

   	for N in Nstatorsbefore:
   		color = np.array(cmhsv(N/14.0))
   		#colorsinbefore.append((color + [1.0,1.0,1.0,1.0])/2)
   		colorsinbefore.append(color)
   		colorsedgebefore.append((color + [0.0,0.0,0.0,0.0])/2)

   	for N in Nstatorsafter:
   		color = np.array(cmhsv(N/14.0))
   		#colorsinafter.append((color + [1.0,1.0,1.0,1.0])/2)
   		colorsinafter.append(color)
   		colorsedgeafter.append((color + [0.0,0.0,0.0,0.0])/2)

   	plt.scatter(np.array(tausafter)/np.array(gammasafter)/(2*np.pi), np.array(tausafter)/np.array(Nstatorsafter),c=colorsinafter,edgecolors=colorsedgeafter)
   	plt.scatter(np.array(tausbefore)/np.array(gammasbefore)/(2*np.pi), np.array(tausbefore)/np.array(Nstatorsbefore),c=colorsinbefore,edgecolors=colorsedgebefore)


   	omega = np.linspace(0.01,300,100)
   	plt.plot(omega,270.0/(1+0.002/theta0*(omega*2*np.pi)),'k-')

  	ax = plt.gca()
   	ax.set_xlabel("$\omega (Hz)$",fontsize=25)
   	ax.set_ylabel("$\langle \\tau \\rangle/N (\mathrm{pN\,nm})$",fontsize=25)
   	ax.set_xlim([0,300])
   	ax.set_ylim([0,350])

   	plt.tight_layout()
   	plt.savefig("avertausingle.png")
   	plt.show()


def Kplotsquare():

 	omegabinsize = 12
 	omegabins = np.arange(0,176,omegabinsize)
 	print omegabins

 	taubinsize = 120
 	taubins = np.arange(0,1751,taubinsize)

 	gammabins = [0,0.28,0.6,1.2,7,15]


	kplotlist={"omega":[],"tau":[],"kin":[],"kout":[],"gamma":[]}

	for iomegabin,omegabin in enumerate(omegabins[:-1]):
		omegabindown=omegabin
		omegabinup=omegabins[iomegabin+1]
#		for itaubin,taubin in enumerate(taubins[:-1]):
		for igammabin,gammabin in enumerate(gammabins[:-1]):
			gammabindown=gammabin
			gammabinup=gammabins[igammabin+1]
			databin = [] # contains all the points that belong to the 2D bin
			for el in data:
				for kk,omegaexp in enumerate(el["traj"]["omega"]):
					omegaexp = omegaexp / (2.0*np.pi)
					if (omegaexp>omegabindown and omegaexp<omegabinup):
						tauexp = el["gamma"]*omegaexp * (2.0*np.pi)
#						if (tauexp>taubindown and tauexp<taubinup):
						if (el["gamma"]>gammabindown and el["gamma"]<gammabinup):
							databin.append([el["traj"]["N"][kk],1.0/el["traj"]["Dt"][kk]])
			print "analysing bin omega= "+str(omegabindown)+" "+str(omegabinup)+"  gamma= "+str(gammabindown)+" "+str(gammabinup)
			print "number of points = "+str(len(databin))
			if len(databin)>2 and len(set(np.array(databin)[:,1]))>1: # more than 2 points and more than 1 different N
				slope, intercept, r_value, p_value, std_err = linregress(databin)
#				kin = 0.001
#				a, cov = curve_fit(linx, np.array(databin)[:,0], np.array(databin)[:,1]-kin*12)
#				print "accepted bin"
				print databin
#				kout = a[0] + kin
				#print slope,intercept
				kin = intercept/11.0 # Nmax=11
				kout = slope + kin
				kplotlist["omega"].append([omegabindown,omegabinup])
#				kplotlist["tau"].append([taubindown,taubinup])
				kplotlist["gamma"].append([gammabindown,gammabinup])
				kplotlist["kin"].append(kin)
				kplotlist["kout"].append(kout)



	print kplotlist
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	cmhsv = plt.get_cmap("viridis")
	colorlist = []

	patchlist = []

	for kk,kout in enumerate(kplotlist["kout"][:-1]):

		patchlist.append(
	    	patches.Rectangle(
	        	(kplotlist["omega"][kk][0],kplotlist["gamma"][kk][0]),   # (x,y)
	        	kplotlist["omega"][kk][1]-kplotlist["omega"][kk][0],          # width
	        	kplotlist["gamma"][kk][1]-kplotlist["gamma"][kk][0],         # height
		))
		colorlist.append(kout)

	p = PatchCollection(patchlist)
	p.set_clim(0,0.01)
	p.set_array(np.array(colorlist))

	ax1.add_collection(p)
	#norm = mpl.colors.Normalize(vmin=0, vmax=2)

	clb = fig1.colorbar(p, ax=ax1)	
	clb.set_label('$k_{out}$', labelpad=20, rotation=0, size = 20)
	fig1.tight_layout()

	plt.xlim([0,200])
#	plt.ylim([0,2000])
	plt.ylim([0,10])

	ax1.set_xlabel("$\omega$ (Hz)")
	ax1.set_ylabel("$\\gamma_l$")
	plt.savefig("koutkinfixed.png")
	plt.show()



def Kplotsingle():

# 	omegabinsize = 12
# 	omegabins = np.arange(0,176,omegabinsize)
# 	print omegabins

 	taubinsize = 120
# 	taubins = np.arange(0,1751,taubinsize)


	marker_palette = sns.color_palette("dark") # Rainbow palette
	edge_palette = sns.color_palette("deep") # Rainbow palette
	
	taus = []
	for dd in data:
		for omega in dd["traj"]["omega"]:
 			taus.append(omega*dd["gamma"])
 			#taus.append(omega/(2.0*np.pi))
 			#taus.append(dd["gamma"])
 	taus = sorted(taus)
	taubins = taus[::len(taus)/8]
	taubins.append(taubins[-1])
	print "taubins"
	print taubins

# 	gammabins = [0,0.28,0.6,1.2,7,15]

	kplotlist={"omega":[],"tau":[],"kin":[],"kout":[],"gamma":[]}

	for itaubin,taubin in enumerate(taubins[:-1]):
			taubindown=taubin
			taubinup=taubins[itaubin+1]
			databin = [] # contains all the points that belong to the 2D bin
			for el in data:
				for kk,omegaexp in enumerate(el["traj"]["omega"]):
					#tauexp = omegaexp / (2.0*np.pi)
					tauexp = el["gamma"]*omegaexp
					#tauexp = el["gamma"]
					if (tauexp>taubindown and tauexp<taubinup):
							DN = int((el['traj']['DN'][kk]))
							sDN = int((el['traj']['DN'][kk])/abs(el['traj']['DN'][kk]))
							if abs(DN)<2:
#								databin.append([el["traj"]["N"][kk],1.0/el["traj"]["Dt"][kk]])							
								for dn in range(0,DN,sDN):
									databin.append([el["traj"]["N"][kk]+dn,1.0/el["traj"]["Dt"][kk],sDN])
			print "analysing bin omega= "+str(taubindown)+" "+str(taubinup)#+"  gamma= "+str(gammabindown)+" "+str(gammabinup)
			print "number of points = "+str(len(databin))
			kinlist = []
			koutlist = []
			kinweightlist = []
			koutweightlist = []
			for N in range(12):
				Kinout = 0
				Kcount = 0
				Kincount = 0
				for dd in databin:
					if dd[0]==N:
						Kinout = Kinout + dd[1]
						Kcount = Kcount + 1
						if dd[2]>0:
							Kincount = Kincount + 1
				if Kcount>7:
					#Kinout = 1.0/Kinout
					Kinout = Kinout/Kcount
					Kin = Kinout*(Kincount*1.0)/Kcount
					print "KK", Kinout, Kin
					if N>0 and Kincount<Kcount:
						koutlist.append((Kinout - Kin)/N)
						koutweightlist.append(Kcount)
					if N<11 and Kincount>0:
						kinlist.append(Kin/(12-N))
						kinweightlist.append(Kcount)
			if kinlist and koutlist:
				kin = np.average(kinlist,weights=kinweightlist)
				kout = np.average(koutlist,weights=koutweightlist)				
	#			if len(databin)>2 and len(set(np.array(databin)[:,1]))>1: # more than 2 points and more than 1 different N
	#				slope, intercept, r_value, p_value, std_err = linregress(databin)
	#				kin = 0.001
	#				a, cov = curve_fit(linx, np.array(databin)[:,0], np.array(databin)[:,1]-kin*12)
	#				print "accepted bin"
	#				print databin
	#				kout = a[0] + kin
					#print slope,intercept
	#				kin = intercept/14.0 # Nmax=11
	#				kout = slope + kin
	#				kplotlist["omega"].append([omegabindown,omegabinup])
				kplotlist["tau"].append((taubindown+taubinup)/2.0)
	#				kplotlist["gamma"].append([gammabindown,gammabinup])
				kplotlist["kin"].append(kin)
				kplotlist["kout"].append(kout)
				databin=np.array(databin)
	#			fig1 = plt.figure()
	#			plt.plot(databin[:,0],databin[:,1],'o')
	#			ax1 = fig1.add_subplot(111)
	#			ax1.set_xlabel("N")
	#			ax1.set_ylabel("Ktotal")
	#			plt.savefig("databin"+str(taubin)+".png")


	print "kplotlist",kplotlist
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	cmhsv = plt.get_cmap("viridis")
	plt.plot(kplotlist["tau"],kplotlist["kout"],'o',
		markeredgecolor=marker_palette[0], markerfacecolor = edge_palette[0],
		markeredgewidth=1, label = "$k_{out}$")
	plt.plot(kplotlist["tau"],kplotlist["kin"],'o',
		markeredgecolor=marker_palette[1], markerfacecolor = edge_palette[1],
		markeredgewidth=1, label = "$k_{in}$")
	plt.legend(fontsize = 20)



#	plt.xlim([0,200])
#	plt.ylim([0,2000])

	ax1.set_xlabel("$\\tau$ (pN nm)",fontsize=20)
	ax1.set_ylabel("$k$",fontsize=20)

	fig1.tight_layout()
	plt.savefig("kouttau.png")
	plt.show()



def KplotN():

# 	omegabinsize = 12
# 	omegabins = np.arange(0,176,omegabinsize)
# 	print omegabins

 	taubinsize = 120
# 	taubins = np.arange(0,1751,taubinsize)


	marker_palette = sns.color_palette("dark") # Rainbow palette
	edge_palette = sns.color_palette("deep") # Rainbow palette

	Nlist = range(13)

# 	gammabins = [0,0.28,0.6,1.2,7,15]

	kplotlist={"omega":[],"tau":[],"kin":[],"kout":[],"gamma":[]}

	Nkinlist = []
	Nkoutlist = []
	kinlist = []
	koutlist = []
	kinweightlist = []
	koutweightlist = []


	for N in Nlist:
		databin = [] # contains all the points that belong to the 2D bin
		for el in data:
			for kN,Nexp in enumerate(el["traj"]["N"]):
				if (N==Nexp):
					DN = int((el['traj']['DN'][kN]))
					sDN = int((el['traj']['DN'][kN])/abs(el['traj']['DN'][kN]))
					if abs(DN)<2:
#						databin.append([el["traj"]["N"][kN],1.0/el["traj"]["Dt"][kN]])							
						for dn in range(0,DN,sDN):
							databin.append([el["traj"]["N"][kN]+dn,abs(DN)/el["traj"]["Dt"][kN],sDN])
		print "analysing bin N= "+str(N)
		print "number of points = "+str(len(databin))
		
		Kinout = 0
		Kcount = 0
		Kincount = 0
		for dd in databin:
			Kinout = Kinout + dd[1]
			Kcount = Kcount + 1
			if dd[2]>0:
				Kincount = Kincount + 1
				if Kcount>5:
					Kinout = Kinout/Kcount
					Kin = Kinout*(Kincount*1.0)/Kcount
					print "KK", Kinout, Kin
		if N>0 and Kincount<Kcount:
			Nkoutlist.append(N)
			koutlist.append((Kinout - Kin)/N)
			koutweightlist.append(Kcount)
		if N<11 and Kincount>0:
			Nkinlist.append(N)
			kinlist.append(Kin/(13-N))
			kinweightlist.append(Kcount)

	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	cmhsv = plt.get_cmap("viridis")
	plt.plot(Nkoutlist,koutlist,'o',
		markeredgecolor=marker_palette[0], markerfacecolor = edge_palette[0],
		markeredgewidth=1, label = "$k_{out}$")
	plt.plot(Nkinlist,kinlist,'o',
		markeredgecolor=marker_palette[1], markerfacecolor = edge_palette[1],
		markeredgewidth=1, label = "$k_{in}$")
	plt.legend(fontsize = 20)



#	plt.xlim([0,200])
#	plt.ylim([0,2000])

	ax1.set_xlabel("N",fontsize=20)
	ax1.set_ylabel("$k$",fontsize=20)

	fig1.tight_layout()
	plt.savefig("koutN.png")
	plt.show()






#def Kplottraj():

#	for el in data:
#		if 






def linx(x,a):
	return a*x



# 	klist = []
# 	for n in range (0,11):
# 		for traj in jumplist:
# 		 	for el in traj:
# 		 		if el[]
# 		# 		if el[0]==n:
# 		# 			ktotal = ktotal + el[1]
# 		# 			if el[2]<0:
# 		# 				numdown = numdown + 1
# 		# 			if el[2]>0:
# 		# 				numup = numup + 1
# 		# print n, ktotal, numup, numdown
# 		#klist.append([n,ktotal/(numup+numdown),(numdown*1.0)/numup])
# 				klist.append([n,])
# 	klist = np.array(klist)
# 	plt.plot(klist[:,0],klist[:,1]/(klist[:,2]+1)/(11.0-klist[:,0]),'o',label="kin")
# 	plt.plot(klist[:,0],klist[:,1]/(1.0/klist[:,2]+1)/(klist[:,0]),'o',label="kout")
# 	plt.legend()
# 	plt.savefig("kinout.png")
# 	plt.show()





#print Neq(0.17, x[0],   2.32715577e-01,   2.05719370e-02,   1.52718402e-04,
#   1.20000050e+01,   2.89668003e-05,   1.74999661e+02)
#print Neq(0.8, 3.16608094e-03,   2.32715577e-01,   2.05719370e-02,   1.52718402e-04,
#   1.20000050e+01,   2.89668003e-05,   1.74999661e+02)
#print Neq(11.3, 3.16608094e-03,   2.32715577e-01,   2.05719370e-02,   1.52718402e-04,
#   1.20000050e+01,   2.89668003e-05,   1.74999661e+02)
