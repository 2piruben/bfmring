
import numpy as np
from scipy.optimize import curve_fit




data = {"300":{"gamma":0.195,"Neq":0,"DN":0,"res":[],"stall":[],"kinR":0.0049,"koutR":0.015,"kinS":0.00218,"koutS":0.0035},
        "300g":{"gamma":0.38,"Neq":0,"DN":0,"res":[],"stall":[],"kinR":NaN,"koutR":NaN,"kinS":0.00203,"koutS":0.00267},
        "500":{"gamma":0.77,"Neq":0,"DN":0,"res":[],"stall":[],"kinR":0.0092,"koutR":0.0095,"kinS":0.00195,"koutS":0.0015},
        "500g":{"gamma":1.6,"Neq":0,"DN":0,"res":[],"stall":[],"kinR":NaN,"koutR":NaN,"kinS":0.002,"koutS":0.00105},
        "1300":{"gamma":10.02,"Neq":0,"DN":0,"res":[],"stall":[],"kinR":0.0056,"koutR":0.00378,"kinS":0.00205,"koutS":0.00064},
        }



def expfuncn(t,N0,Ninf,mu):
	return Ninf + (N0-Ninf) * np.exp(-mu*t)

def fitexp(expfunc,tdata,Ndata,Nmax=14,N0 = None, Ninf = None):
	prange = [(0,inf),(0,inf),(0,inf)]
	if N0:
		prange[0] = (N0,N0)
	if Ninf:
		prange[1] = (Ninf,Ninf)
	popt,pcov = curve_fit(expfunc,tdata,Ndata,bounds=prange) 

	kinL = popt[0]*popt[2]/Nmax
	koutL = popt[2]-kinL





