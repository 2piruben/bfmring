#include "ringdisc.h"
#include "ringcont.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;


int N = 14; // number of binding sites
int width = 1; // binding sites occupied per stator

double length = width/N; // fration of the ring occupied per stator

double kin0=1.0/1000.0; // (1.0/800) basal binding rate per free side/length (per second)
double kout0=kin0*(N-9)/(9); // stall dettaching rate (per second), chosen to reach eq. at Nocc = 9
//double koutr=1.2E-6; // dettaching rate rate with vel (per second) computed from traces without conservative torque
double koutoff = 3.3E-2; // 4.14E-2
double kouton = koutoff*8.4E-3; 
//double kouton = kin0*(N-9)/(9)*0.05; // dettaching rate when active per free side/length (per second)
//double koutoff = kin0*2.0; // dettaching rate when not active per free side/length (per second)
	
//double tdwell = 0.0001; // average time of chemical waiting for an ion
double tdwell = 5.9E-6; // 5.4e-6

double theta0 = 2*M_PI/27.0; // angular distance advanced per ion
//double taustator = 150; // internal torque exherted per stator
double taustator = 137; // 120

double tim=0; // current time
double timeaux=0; // dummy time counter
double warmtime = 0; // warming time before starting to record
double maxtim = 7000; // max simulation time (5500 in trajs)
double tstall1 = 3500 ; // time to start the stall of the motor (3500 for resurrections)
double tstall2 = 3800 ; // time to stop the stall of the motor (5 min later = 3800)
double nextstalltim = tstall1; //  next time to change protocol
int fstall = 1 ; // flag to set the stall pulse regime
double deltatim = 0; // aux variable for time

double gl = 0.8; // friction coefficient of the load 
// g300=0.17 pN nm s  g500=0.8 pN nm s g1300=11.3 pN nm s
double vel = 0; // velocity of the motor

double tauin = 0; // internal torque exherted by all the stators

double tauex = 0; // external torque applied
double Nbind = 0; // aux counter

//double kout(int vel){
//	return kout0+vel*koutr;
//}

int main(){

//ringdisc ring(N, width, kin0, kouton); // creation of the ring
ringcont ring(N, kin0, kout0); // creation of the ring

//ring.SetHistogram(); // creation of the histogram 

ofstream outtraj;
ostringstream filetitle;


//for(int igl=0;igl<3;igl++){
//	if (igl==0) {gl = 0.17; Nbind = 3.4;}
//	if (igl==1) {gl = 0.8; Nbind = 5.7;}
//	if (igl==2) {gl = 11.3; Nbind = 8.5;}

//double tsts[] = {0,60,300,900,1800}; 

//for(int its=0; its<=4; its++){
//	tstall1 = tstall2 - tsts[its];

//for(int ngl=0; ngl<10; ngl++){
//	gl = 0.1 * pow(10,ngl*0.2);
//


for(float kin = 0.0005;kin<0.05;kin +=0.0015){
for(float kout = 0.0000001;kout<0.01;kout +=0.0005){

	ring.SetKin(kin);
	ring.SetKout(kout);


//filetitle<<"output/"<<"output.out";

//kin0 = 4.2/800; // 1/800 for l=1, 5/800 for l=2  12/800 for l=inf
//koutr=4.2*1.2E-6; // 1.2E-6 for l=1 dettaching rate rate with vel (per second) computed from traces without conservative torque
//ring.SetKin(kin0);


//	ring.SetChemMech(tdwell,theta0,taustator,gl);
//	ring.SetCatchBond(kouton,koutoff);

//ring.SetVerbose(); // small loop to try realisation steps
//for (int i=0;i<10;i++){
//	ring.PrintRing();
//	ring.ChemoMechReactionStep();
//}


for(int i=0;i<50;i++){ // Number of different realisations

	ring.SetRing(EMPTY_RING);

	filetitle.str("");
	filetitle<<"output/in"<<kin<<"out"<<kout<<"trajectory"<<i<<".in";
	outtraj.open(filetitle.str());
	//outtraj<<"gl "<<gl<<'\n';
	outtraj<<"realisation "<<i<<'\n';		
	//outtraj<<"tstall1 "<<tstall1<<'\n';
	//outtraj<<"tstall2 "<<tstall2<<'\n';
	outtraj<<"kin "<<kin<<'\n';
	outtraj<<"kout "<<kout<<'\n';
	outtraj.close();
	filetitle.str("");
	filetitle<<"output/in"<<kin<<"out"<<kout<<"trajectory"<<i<<".out";
	outtraj.open(filetitle.str());

	tim=0;
	fstall = 1;
	nextstalltim = tstall1;
	timeaux = 0; 
	while(tim<warmtime){ // warming run prior to start recording and setting dynamics
		//tauin = ring.GetOccupancy()*taustator;
		//tauin = Nbind*taustator;
		//vel = (tauin)/gl;
		
		//vel = theta0/2/tdwell*(-1+sqrt(1+4*ring.GetOccupancy()*taustator*tdwell/theta0/gl));
		//ring.SetKout(koutoff-(koutoff-kouton)/(1+tdwell*vel/theta0));

//		ring.SetKout(kout(vel));
		deltatim = ring.ReactionStep();
		tim += deltatim;
	}

	tim = 0; // restart timer to start the record
	timeaux = 0;
	ring.SetRing(EMPTY_RING);
	vel = 0;

	//outtraj<<tim<<' '<<ring.GetOccupancy()<<' '<<ring.MeasureVacancy()<<' '<<ring.GetVelocity()<<' '<<ring.MeasureDutyRatio()<<'\n'; 
	//outtraj<<tim<<' '<<ring.GetOccupancy()<<' '<<vel<<' '<<1.0/(1+tdwell*vel/theta0);
	//outtraj<<' '<<koutoff-(koutoff-kouton)/(1+tdwell*vel/theta0)<<'\n'; 
	while(tim<maxtim){ // recorded experiment
		//cout<<"Starting reaction at time: "<<tim<<'\n'; 
//		tauin = ring.GetOccupancy()*taustator;
		//tauin = Nbind*taustator;
		//ring.SetExtTorque(0);
		//vel = theta0/2/tdwell*(-1+sqrt(1+4*ring.GetOccupancy()*taustator*tdwell/theta0/gl));
		





		//ring.SetKout(koutoff-(koutoff-kouton)/(1+tdwell*vel/theta0));

//		vel = (tauin + tauex)/gl;
//		ring.SetKout(kout(vel));

		deltatim = ring.ReactionStep(nextstalltim-tim);

		if (deltatim > 0){
			tim += deltatim;
		}
		else if (deltatim < 0 and fstall == 1){
//			ring.SetVerbose(false);
			tim = nextstalltim;
			nextstalltim = tstall2;
			fstall++;
			ring.SetKout(0.000000000001);
		}
		else if (deltatim < 0 and fstall == 2){
//			ring.SetVerbose(false);
			tim = nextstalltim;
			nextstalltim = INFINITY;
			fstall++;	
			ring.SetKout(kout);
		}

		//	The velocity is changed again because the ring status could have changed (e.g. stall)
		// during the fstall options switch
		//vel = theta0/2/tdwell*(-1+sqrt(1+4*ring.GetOccupancy()*taustator*tdwell/theta0/gl));
		//if (fstall == 2){
		//	ring.SetStall();
		//	vel = 0;
		//}

		if (tim>timeaux){
			outtraj<<tim<<' '<<ring.GetOccupancy()<<'\n';//<<' '<<vel<<' '<<1.0/(1+tdwell*vel/theta0);
			//outtraj<<' '<<koutoff-(koutoff-kouton)/(1+tdwell*vel/theta0)<<'\n'; 
			timeaux = tim+5; // record at least every second
			if (false){
				cout<<"time: "<<tim<<'\n';
//				ring.PrintRing();
			}
		}
 	}
 	outtraj.close();
// 	Nbind +=ring.GetOccupancy();
}
}	//outtraj<<kintloop<<' '<<Nbind/1000.0<<'\n'; 


}}


