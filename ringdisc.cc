//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// 
// CLASS TO SIMULATE A DISCRETE RING BINDING AND UNBINDING
// 
// Ruben Perez Carrasco                                  8-11-16
//
//////////////////////////////////////////////////////////////////

/* This class models the binding and unbinding of stators to the ring
taking into account that the stators can bind the ring in discrete positions around
the ring

The ring has a number of sites "ringlength" and the stators a length "statorlength" so in the optimal case
the ring will have attached "ringsites/dimerlength" number of stators.

To compute the positions at which there is enough space to bind a stator, the set of empty sites
that allow for a whole unit to attach is stored in "eset"

In a simlar way the bound stators are stored in "oset" (occupied set)

Reactions are computed using the Gillespie algorithm where the propensity of detaching is kout*N and
the propensity of attaching is kin*(number of empty areas).
*/


#include "ringdisc.h"
#include <math.h>

//// CONSTRUCTORS AND DESTRUCTOR

ringdisc::ringdisc(int rl, int ul,float ki, float ko) //
// constructor of the ring of length l with kin and kout rates 
	{
	rlength = rl;
	ulength = ul;
	kin = ki; // rate of attaching
	koutoff = ko; // rate of detaching inactive units 
	kouton = ko; // rate of detacching active units
	kout = ko;
	angle = 0;
	turns = 0;
	tauext = 0; // external torque applied
	f_stall = false;  // flag to consider an assay at stall

	// Initially the ring will be empty
	for (int i=0; i<rlength; i++) eset.push_back(i); 

	r = gsl_rng_alloc (gsl_rng_mt19937);	// r contains the random numbers
	SEED = -1;
	if (SEED<1){ // if the SEED is negative, it is generated from clock
		SEED = time(NULL);
	}
	gsl_rng_set (r,SEED);
	}

ringdisc::~ringdisc()
	{
	}

///// MEMBER FUNCTIONS

void ringdisc::SetRing(int n){//	
// resets de ring in a empty or full state with kin and kout rates
// n can be EMPTY_RING or FULL_RING

	eset.clear();
	oset.clear();
	angle = 0; // it also sets the angle to zero
	turns = 0;

	switch(n){
	
		case(EMPTY_RING): // No units initially attached
			for (int i=0; i<rlength; i++) eset.push_back(i); 
		break;

		case(FULL_RING): // Ring initially full with stators
			for (int i=0; i<(rlength-ulength+1); i+=ulength) oset.push_back(stator(i));
		break;

		default:
			cout<<"SetRing invalid argument\n";
			exit(1);
	}	
}

void ringdisc::PrintOccupancy(ofstream &output){
// print positions of the bound stators to output	
	for(oit=oset.begin();oit!=oset.end();++oit) output<<(*oit).position <<' ';	
//	cout<<'\n';
}

void ringdisc::PrintVacancy(ofstream &output){
// print positions and length of empty regions in the ring
	for(eit=eset.begin();eit!=eset.end();++eit) output<<*eit<<' ';	
}

void ringdisc::PrintOccupancy(){
// print positions of the bound stators to output	
	for(oit=oset.begin();oit!=oset.end();++oit) cout<<(*oit).position<<' ';	
	cout<<'\n';
}

void ringdisc::PrintVacancy(){
// print positions and length of empty regions in the ring
	for(eit=eset.begin();eit!=eset.end();++eit) cout<<*eit<<' ';	
	cout<<'\n';
}

int ringdisc::GetOccupancy(){
// 	print number of bound stators
	return oset.size();
}

void ringdisc::SetKin(double k){
// set the rate of attaching
	kin=k;
}
	
void ringdisc::SetKout(double k){
// set the rate of detaching
	kout=k;
}

void ringdisc::SetChemMech(double tc, double ang, double tau0, double gamm){
	tchem = tc;
	singletau = tau0;
	mechangle = ang;
	gamma = gamm;
}


void ringdisc::SetCatchBond(double kon, double koff){
	kouton = kon;
	koutoff = koff;
}

void ringdisc::SetStall(){
	f_stall = true;
}

void ringdisc::SetExtTorque(double exttau){ // no argument to let it free
	f_stall = false;
	tauext = exttau;
}

void ringdisc::SetVerbose(bool verb){
	verbose=verb;
}

double ringdisc::GetVelocity(){
	if (f_stall==true) return 0;
	else return (MeasureActivatedStators()*singletau+tauext)/gamma;
}

double ringdisc::ReactionStep(double maxtau){
//	makes a reactionstep. If it requires a time that is bigger than maxtau, it does not perform the step
//  and returns a negative time
	space=MeasureVacancy();	// space avaiable to attach an unit
	propin=space*kin/ulength; // propensity of attaching
	propout=oset.size()*kout; // propensity of detaching propostional to number of bound stators
	rnd1=gsl_ran_flat (r,0,1);
	rnd2=gsl_ran_flat (r,0,1);
	tau=1.0/(propout+propin)*log(1.0/rnd1); // time of next reaction
	if (tau<maxtau){
		if (verbose){
		cout<<"RS: Propensity in: "<<propin<<"      Proprensity out: "<<propout<<'\n';
		cout<<"RS: rnd1 "<<rnd1<<'\n';
		cout<<"RS: time "<<tau<<'\n';
		}
		if ((propout/(propin+propout))>rnd2) DettachingEvent(); // selection of the reaction (dettaching or attaching)
		else AttachingEvent();
		return tau;
	}
	else return tau*(-1.0);
}

double ringdisc::ChemoMechReactionStep(double maxtau){
//	Is the same as ReactionStep but also takes into account that activation and deactivation of the rotors can take place
// computing mechanical and chemical times

	propin = MeasureVacancy()*kin/ulength; // propensity of attaching
	propouton = kouton*MeasureActivatedStators(); // propensity of detaching a pushing stator
	propoutoff = koutoff*MeasureDeactivatedStators(); // propensity of detaching a dwell stator
	propchem= MeasureDeactivatedStators()/tchem; // propensity of activating one stator
	rnd1=gsl_ran_flat (r,0,1);
	rnd2=gsl_ran_flat (r,0,1);
	tau=1.0/(propouton+propoutoff+propin+propchem)*log(1.0/rnd1); // time of next reaction

	if (verbose){
		cout<<"RS: Propensity in: "<<propin<<"   Proprensity out: "<<propouton+propoutoff<<"   Proprensity activation: "<<propchem<<'\n';
		cout<<"RS: rnd1 "<<rnd1<<'\n';
		cout<<"RS: time "<<tau<<"   of maximum "<<maxtau<<'\n';
	}

	if (DeactivationEvent(min(tau,maxtau))) { // if there is a Deactivation event, add that time and try again
		if (verbose){
			cout<<"Mechanical deactivation occurs first \n";
		}
		if (f_stall == false) angle += (singletau * (MeasureActivatedStators() + 1) + tauext)/ gamma * nexttaumech; // advance rotor until mechanical dectivation position
		return nexttaumech;
		}

	else if (tau<maxtau){
		if ((propouton/(propin+propoutoff+propouton+propchem))>rnd2){
			advancerotor(tau);
			DettachingEvent(S_ON); // selection of the reaction (dettaching or attaching)
		}
		else if (((propouton+propoutoff)/(propin+propouton+propoutoff+propchem))>rnd2){
			advancerotor(tau);
			DettachingEvent(S_OFF);
		}	
		else if (((propouton+propoutoff+propin)/(propin+propouton+propoutoff+propchem))>rnd2){
			advancerotor(tau);
			AttachingEvent();
		} 
		else{
			advancerotor(tau);
			ActivationEvent();
		}
		return tau;
	}
	else return tau*(-1.0); // any possible event does not occur before maxtau
}
	
double ringdisc::DettachingEvent(int state){
	if (verbose) cout<<"DE: Starting detachment\n";
	RandomDettaching(state); // A random element is selected to be detached
	if (verbose) cout<<"DE: Detaching from position "<<(*oit).position<<"\n";
	// this function sets the "oit" pointer to that element
	// Now we find the previous and posterior empty regions and set their respective pointers "eitmin" and "eitmax"
	// to eventually fuse them in a new empty region

	// First of all we add the new emptied space to eset
	eset.push_back(oit->position);

	// Then we check if there are new spaces to the left of
	// the emptyset that would require inclusion
	icount = 0;
	for (icount = 0; icount<(ulength-1); icount++){
		for(oittemp=oset.begin(); oittemp!=oset.end(); ++oittemp){
			if (oittemp->position == inring((oit->position)-ulength-icount)){
				break;
			}
		}
		if  (oittemp == oset.end()){
			eset.push_back(inring((oit->position)-icount-1));
				if (verbose) cout<<"AE inserting: "<<inring((oit->position)-icount-1)<<"\n";
		}
		else{ break;}
	}

	// Then we check if there are new spaces to the right of
	// the emptyset that would require inclusion
	icount = 0;
	for (icount = 0; icount<(ulength-1); icount++){
		for(oittemp=oset.begin(); oittemp!=oset.end(); ++oittemp){
			if (oittemp->position == inring((oit->position)+icount+ulength)){
				break;
			}
		}
		if  (oittemp == oset.end()){
			eset.push_back(inring((oit->position)+icount+1));
				if (verbose) cout<<"AE inserting: "<<inring((oit->position)+icount+1)<<"\n";
		}
		else{ break;}
	}
	
	id1 = oit->position;
	// Finally we remove the chosen unit
	oset.erase(oit);
	return id1;
}
	
double ringdisc::AttachingEvent(){
	if (verbose) cout<<"AE: Starting Attachment"<<'\n';
	RandomAttaching();
	// This function sets *eit to the first position of 
	// the selected  empty slot
	if (verbose) cout<<"AE: Attaching to position "<<*eit<<'\n';

	// First we add the new position to the occupied list
	oset.push_back(stator(*eit));
	id1 = *eit;
	if (verbose) cout<<"AE id1: "<<id1<<"\n";

	// The binding of an unit removes a set of possible places 
	// that are added to eit
	for (icount = -1*ulength+1 ; icount < ulength; icount++){
		if (verbose) cout<<"AE removing from empty: "<<inring(icount+id1)<<"\n";
		for(eittemp=eset.begin(); eittemp!=eset.end(); ++eittemp){
			if (*eittemp == inring(icount+id1)){
				eset.erase(eittemp);
				break;
			}
		}
	}

	// Finally we remove the empty space occupied
	return id1;
}

void ringdisc::RandomDettaching(int state){
// RandomDettaching of a stator with a certain chemical state	
	if (state == S_ALL){
		RandomDettaching();
	}
	else{
		if (state == S_ON) id1 = MeasureActivatedStators();
		else if (state == S_OFF) id1 = MeasureDeactivatedStators();
		// Function to choose one random stator to detach from oset. It sets the dummy pointer "oit" to it	
		rnd3 = gsl_rng_uniform_int(r,id1);
		oit = oset.begin();
		icount = 0;
		while (icount<rnd3 || oit->status != state){
			if (oit->status == state) icount ++;
			oit++;
		}
	}
}

void ringdisc::RandomDettaching(){
	// Function to choose one random stator to detach from oset. It sets the dummy pointer "oit" to it
	rnd3=gsl_rng_uniform_int(r,oset.size());
	oit = oset.begin();
	advance(oit,rnd3);
}

void ringdisc::RandomAttaching(){
	// Function choosing the position of the ring to a new attachment.
	rnd3=gsl_rng_uniform_int(r,eset.size());
	eit = eset.begin();
	advance(eit,rnd3);
}

double ringdisc::MeasureVacancy(){
	// returns the total empty length of the ring available to bind a stator. 
	// This is used to measure how probable is to bind a stator
	return eset.size(); 
	// Note that more complex measures can be defined
	// taking into account the number of free sites
	// instead of the number of possible binding configurations
}

double ringdisc::MeasureDeactivatedStators(){
	// returns the total number of stators that are currently not mechanically working
	icount = 0;
	for(oit=oset.begin();oit!=oset.end();++oit){
		if (oit->status == S_OFF){
			icount++;
		}
	}
	return icount; 
}

double ringdisc::MeasureActivatedStators(){
	// returns the total number of stators that are currently mechanically working
	icount = 0;
	for(oit=oset.begin();oit!=oset.end();++oit){
		if (oit->status == S_ON){
			icount++;
		}
	}
	return icount; 
}

double ringdisc::MeasureDutyRatio(){
	// returns the fraction of stators activated
	return MeasureActivatedStators()/(MeasureActivatedStators()+MeasureDeactivatedStators());
}

bool ringdisc::DeactivationEvent(double maxtau){
	// checks when is the next stator deactivating, and if it occurs before maxtau, then 
	// it proceeds to the deactivation of that stator
	const static stator* deactstp; // pointer to the next deactivation stator
	// it is const because it points to a <set>, which are immutable
	nexttaumech = numeric_limits<double>::infinity(); //  in principle it will never occur
	if (f_stall == false){ // if not at stall then the time to reach a mechanical step may not be infinite
		for(oit=oset.begin(); oit!=oset.end(); ++oit){
			if (oit->status == S_ON){
				taumechaux = (*oit).endangle - angle;
//				if (taumechaux < 0) taumechaux += 2*M_PI;
				if (taumechaux < nexttaumech){ 
					nexttaumech = taumechaux;
					deactstp = &(*oit); // <- when you think you understand c++... then this happens
				}
			}
		}
	}
	if (!isinf(nexttaumech)){
	nexttaumech = nexttaumech * gamma/(singletau*MeasureActivatedStators() + tauext); // transform angle into time
	}

	if (verbose){
		cout<<"Next deactivation in angular distance: "<<nexttaumech<<' ';
		cout<<"( time: "<<nexttaumech<<")\n";
	}
	if (nexttaumech < maxtau){ //  if next time occurs before tau then deactivate it
		(*deactstp).status = S_OFF; 
		return true;
	}
	else return false;
}

double ringdisc::ActivationEvent(){
	if (verbose) cout<<"AE: Starting Activation"<<'\n';
	rnd3 = gsl_rng_uniform_int(r,MeasureDeactivatedStators());
	// This function sets *eit to the first position of 
	// the selected  empty slot
	oit = oset.begin();
	icount = 0;
	while (icount<rnd3 || oit->status == S_ON){ // search of rnd3-th deactivated stator and point to it with oit
		if (oit->status == S_OFF){
			icount ++;
		}
		oit++;
	}

	oit->status = S_ON; // activate it
	oit->endangle = angle + mechangle; // end angle will be current angle plus angle advance per ion

	return (oit->position);
} 


int ringdisc::ringdistance(int b,int a){
// Histogram 
	int x;
	if((b-a)>0) x=b-a;
	else x=b-a+rlength;
	return x;
}

int ringdisc::inring(int a){
	int x=a;
	while(x>=rlength) x-=rlength;
	while(x<0) x+=rlength;
	return x;

}

void ringdisc::advancerotor(double dt){
// advance the angular position of the ring a timestep dt. 
// correction corrects the number of acitvated stators counted in the step
	if (f_stall == false){ 
		angle += (singletau * MeasureActivatedStators() + tauext)/ gamma * tau;
		updateangle();
	}
}

void ringdisc::updateangle(){
// since high precission is necessary in the angle (compared to the amount of turns given)
// it is easier to measure the angle in two different magnitudes, one that counts large number of turns 
// and another that computes a more precise angle
	while (angle>2*M_PI){
		angle -= 2*M_PI;
		for(oit=oset.begin(); oit!=oset.end(); ++oit){
			if (oit->status == S_ON) oit->endangle -= 2*M_PI;
		}
		turns++ ; 
	}	
}
		
void ringdisc::PrintRing(){
// Function that returns the actual info of the ring
	cout<<"\n";
	cout<<"------";
	cout<<"Ring located at angle: "<<angle<<'\n';
	cout<<" Attached stators: "<< oset.size();
	cout<<" ("<<MeasureActivatedStators()<<" ON/ "<<MeasureDeactivatedStators()<< " OFF)\n";
	cout<<" Possible attachment sites: "<<MeasureVacancy()<<'\n';
	cout<<" List of empty sites: ";
	PrintVacancy();
	cout<<" List of occupied sites: \n";
	for(oit=oset.begin(); oit!=oset.end(); ++oit){
		cout<<"   position: "<<oit->position<<" ";
		cout<<"   status: "<<oit->status<<" ";
		if (oit->status == S_ON) cout<<"   endangle: "<<oit->endangle<<"\n";
		else if (oit->status == S_OFF) cout<<"   activation at: "<<oit->activationtime<<"\n";
	}
	cout<<"\n\n";
}



