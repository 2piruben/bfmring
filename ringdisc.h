
# ifndef RINGDISC_H
   # define RINGDISC_H


#include <string>
#include <set>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <gsl/gsl_randist.h>
#include <limits>
#include "stator.h"


using namespace std;


#define FULL_RING 0
#define EMPTY_RING 1



class ringdisc{


public:

	ringdisc(int rl, int ul, float in, float out); // Constructor of the ring with size rl and unit length ul, and reaction constants in and out
	~ringdisc(); // Destructor
	void SetRing(int n); // Set the total occupation of the Ring to FULL_RING or EMPTY_RING
	void PrintOccupancy(ofstream &out); // Print occupancy status 
	void PrintVacancy(ofstream &out); // Print vacant positions 
	void PrintOccupancy(); // Print occupancy status to standard output
	void PrintVacancy(); // Print vacant positions to standard output

	int GetOccupancy(); // Get the number of stators attached
	double GetVelocity(); // Get angular velocity of the rotor (rad/s)
	void SetKin(double k); // Set the value of kin;
	void SetKout(double k); // Set the value of kout;
	void SetChemMech(double tchem, double mechangle, double singletau, double gamma);
	void SetCatchBond(double kouton, double koutoff);
	void SetStall();
	void SetExtTorque(double exttau=0);
	void SetVerbose(bool verb=true);
	double AttachingEvent(); // Attaching of a unit to the ring
	double DettachingEvent(int state=S_ALL); // Dettaching of a unit in state state from the ring
	double ActivationEvent(); // Activation of a stator
	double MeasureVacancy(); // Available area for a dock
	double MeasureDeactivatedStators(); // Available area for a dock
	double MeasureActivatedStators(); // Available area for a dock
	double MeasureDutyRatio(); // Available area for a dock
	double ReactionStep(double maxtau = INFINITY); // Realization of one reaction
	double ChemoMechReactionStep(double maxtau = INFINITY); // Realization of reaction or activation of stators
	bool DeactivationEvent(double maxtau = INFINITY); // perform the most probable deactivation event if it occurs before maxtau

	void PrintRing();


	
private:

	int rlength; // length of the ring
	int ulength; // length of the unit	
	vector <stator> oset; // Occupation vector is a vector of stators
	vector <int> eset; // Empty set is a vector of empty spaces
	vector <stator>::iterator oit, oittemp; // iterator for elements of oset
	vector <int>::iterator  eit, eittemp; // iterator for elements of eset
	double kin; // single molecule reaction rate of attaching
	double kout; // single molecule reaction rate of dettaching
	double kouton; // mechanically activated stator rate of dettaching
	double koutoff; // mechanically inactive stator rate of dettaching
	double rnd1; // random number for reaction selection
	double rnd2; // random number for reaction time
	double angle; // current angular position of the rotor
	double turns; // total number of turns made
	int rnd3; // random number for chosing dettaching
	double space; // empty space available
	double propin; // propensity of attaching reaction
	double propout; // propensity of dettaching reaction
	double propouton; // propensity of dettaching reaction for mech active stator
	double propoutoff; // propensity of dettaching reaction for mech inactive stator
	double propchem; // propensity of activating an attached stator
	double tau; // last reaction time
	double nexttaumech; // next mechanical deactivation time
	double taumechaux; // aux var for nexttaumech
	double singletau; //  torque applied per stator (torque/gamma)
	bool f_stall; // flag to check if the motor is stalled
	double tchem; // average time for an ion to reach an empty stator
	double mechangle; // angular distance pushed by an activated stator per ion
	double tauext; // external torque applied on the ring (negative to hinder)
	double gamma; // friction of the load attached
	int icount; // auxiliar int
	int id1 ; // auxiliar position counter
	int SEED; // rng SEED, if negative, it is taken from clock
	gsl_rng * r; // container for the random numbers
	int ringdistance(int a,int b); // returns the angular position x inside the ring (0,length)
	int inring(int a); // returns absolute position in the ring
	void RandomDettaching(); // Selects a random member of oset an points oit to it;
	void RandomDettaching(int); // Selects a random member of active or inactive oset an points oit to it;
	void RandomAttaching(); // Selects a random member of oset an points oit to it;
	void updateangle(); // keep angle in the circle
	void advancerotor(double tau); // advance rotor a certain time
	bool verbose = false;
};


#endif
