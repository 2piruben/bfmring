# README #

## STATOR BINDING IN THE BFM RING ##

Set of programs simulating different binding/unbinding properties of the BFM stators to the FliG ring. The simulation of binding and unbinding properties are simulated with the following two classes

* **ringdisc** Simulates the flig ring as having a discrete number of binding sites. Each stator can span one or several binding sites. 

* **ringcont**  Simulates the flig ring (or the peptidoglican membrane) as continuous binding surfcae. Each stator can bind continuously anywhere along the ring. 

Both classes share functions with the same names to simulate the binding dynamics but there is one main different. In **ringdisc**, the ring is defined by two quantities `ringlength` which sets the number of binding sites and `statorlength` the number of binding sites that occupies each individual stator. On the other hand, **ringcont** just requires the quantity `length` that defines the fraction of the whole ring that occupies one stator.  

The simulation is run by the program **BFMrun** and the occupation of the ring is simulated with **plotirng** (this last one requires revision)

# Library dependencies

The dynamics of binding and unbinding make use of the GNU Scientific Libraries ([GSL](https://www.gnu.org/software/gsl/)) to generate the necessary random numbers. The program `plotring` makes use of the PNGwriter libraries that can be downloaded [here](http://pngwriter.sourceforge.net/) 





